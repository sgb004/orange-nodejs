/**
 * @author @sgb004
 * @version 0.1.1
 */

module.exports.functions = {
	setValue: function(value){
		this['isObject'] = true;
		if( typeof(value) != 'object' ){
			this.isObject = false;
			value = {};
		}
		this.value = Object.assign({
			'column': '',
			'dir': ''
		}, value);
	},
	isEmpty: function(){
		var isEmpty = false;

		this.value.column = parseInt(this.value.column);
		if( this.value.column < 0 || isNaN(this.value.column) ){
			if( this.required ){
				this.addError({ 'column': ['Please, select a column 1.']});
			}
			isEmpty = true;
		}

		if( typeof( this.value.dir ) != 'string' ){ this.value.dir = ''; }
		this.value.dir = this.value.dir.trim();
		if( this.value.dir == '' ){
			if( this.required ){
				this.addError({ 'dir': ['Please, select the direction of the order of the column.']});
			}
			isEmpty = true;
		}

		this['_isEmpty'] = isEmpty;
		return isEmpty;
	},
	validate: function() {
		var isValid = true;

		if( this.value.dir != '' ){
			var dirTester = new RegExp('^(asc|desc)$');
			if(!dirTester.test(this.value.dir) ){
				this.addError({ 'dir': ['Please, select a valid direction asc or desc']});
				isValid = false;
			}
		}

		//
		if( (this.value.column < 0 || isNaN(this.value.column)) && isValid && this.value.dir != '' ){
			this.addError({ 'column': ['Please, select a column.']});
			isValid = false;
		}
		return isValid;
	}
};