/**
 * @author @sgb004
 * @version 0.1.0
 */

module.exports.functions = {
	setValue: function(value){
		this['isObject'] = true;
		if( typeof(value) != 'object' ){
			this.isObject = false;
			value = {};
		}
		this.value = Object.assign({
			'value': '',
			'regex': ''
		}, value);
	},
	isEmpty: function(){
		var isEmpty = false;

		if( typeof( this.value.regex ) != 'string' ){ this.value.regex = ''; }
		this.value.regex = this.value.regex.trim();
		if( this.value.value == '' ){
			if( this.required ){
				this.addError({ 'value': ['Please, write a search value.']});
			}
			isEmpty = true;
		}

		if( typeof( this.value.regex ) != 'string' ){ this.value.regex = ''; }
		this.value.regex = this.value.regex.trim();
		if( this.value.regex == '' ){
			if( this.required ){
				this.addError({ 'regex': ['Please, select if the column should be treated as regular expression.']});
			}
			isEmpty = true;
		}

		this['_isEmpty'] = isEmpty;
		return isEmpty;
	},
	validate: function() {
		var isValid = true;
		if( this.value.regex != 'false' && this.value.regex != '' && isValid && this.value.value == '' ){
			this.addError({ 'value': ['Please, write a search value.']});
			isValid = false;
		}
		return isValid;
	}
};