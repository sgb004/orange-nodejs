/**
 * @author @sgb004
 * @version 0.1.0
 */

var ColumnType = function(){
	this.value = {};
	this.errors = [];
	this.required = true;
}
var functions = require('./ColumnType');
ColumnType.prototype = functions.functions;
ColumnType.prototype.addError = function( notice ){this.errors.push( notice );}
ColumnType.prototype.getErrors = function( notice ){return this.errors;}

module.exports.functions = {
	setValue: function(value){
		this['isObject'] = true;
		if( typeof(value) != 'object' ){
			this.isObject = false;
		}
		this.value = value;
	},
	isEmpty: function(){
		var isEmpty = false;
		var value = {};
		var length = 0;
		var i;
		if( this.isObject ){
			value = this.value;
		}

		for( i in value ){
			length++;
			break;
		}

		if( length == 0 ){
			isEmpty = true;
			if( this.required ){
				this.addError(this.notice);
			}
		}
		this['_isEmpty'] = isEmpty;
		return isEmpty;
	},
	validate: function() {
		var isValid = true;
		if( !this._isEmpty ){
			if( this.isObject ){
				var column, c, fieldErrors;
				var errors = {};
				for( c in this.value ){
					fieldErrors = [];
					column = new ColumnType();
					column.setValue(this.value[c]);
					this.value[c] = column.value;
					if( column.isEmpty() ){
						fieldErrors = column.getErrors();
					}else if( !column.validate() ){
						fieldErrors = column.getErrors();
					}
					if( fieldErrors.length > 0 ) {
						isValid = false;
						errors[c] = [ fieldErrors ];
					}
				}
				this.addError(errors);
			}else{
				isValid = false;
				this.addError('Please, add columns.');
			}
		}
		return isValid;
	}
};