/**
 * @author @sgb004
 * @version 0.1.0
 */

var SearchType = function(){
	this.value = {};
	this.errors = [];
	this.required = false;
}
var functions = require('./SearchType');
SearchType.prototype = functions.functions;
SearchType.prototype.addError = function( notice ){this.errors.push( notice );}
SearchType.prototype.getErrors = function( notice ){return this.errors;}

module.exports.functions = {
	setValue: function(value){
		this['isObject'] = true;
		this.searchType = new SearchType();
		if( typeof(value) != 'object' ){
			this.isObject = false;
			value = {};
		}

		this.value = Object.assign({
			'data': '',
			'name': '',
			'searchable': 'true',
			'orderable': 'true',
			'search': {}
		}, value);

		this.searchType.setValue(this.value.search);
		this.value.search = this.searchType.value;
	},
	isEmpty: function(){
		var isEmpty = this.searchType.isEmpty();

		if( isEmpty ){
			var errors = this.searchType.getErrors();
			if( errors.length > 0 ){
				this.addError({ 'search': this.searchType.getErrors()});
			}
		}

		if( typeof( this.value.data ) != 'string' ){ this.value.data = ''; }
		this.value.data = this.value.data.trim();
		if( typeof( this.value.name ) != 'string' ){ this.value.name = ''; }
		this.value.name = this.value.name.trim();

		if( this.value.data == '' && this.value.name == '' ){
			if( this.required ){
				this.addError({ 'data': ['Please, define a column data.']});
			}
			isEmpty = true;
		}

		if( typeof( this.value.searchable ) != 'string' ){ this.value.searchable = ''; }
		this.value.searchable = this.value.searchable.trim();
		if( this.value.searchable == '' ){
			if( this.required ){
				this.addError({ 'searchable': ['Please, select if the column is searchable.']});
			}
			isEmpty = true;
		}

		if( typeof( this.value.orderable ) != 'string' ){ this.value.orderable = ''; }
		this.value.orderable = this.value.orderable.trim();
		if( this.value.orderable == '' ){
			if( this.required ){
				this.addError({ 'orderable': ['Please, select if the column is orderable.']});
			}
			isEmpty = true;
		}

		this['_isEmpty'] = isEmpty;
		return isEmpty;
	},
	validate: function() {
		var isValid = this.searchType.validate();
		var booleanTester = new RegExp('^(true|false)$');

		if( !isValid ){
			this.addError({ 'search': this.searchType.getErrors()});
		}

		if( this.value.searchable != '' ){
			if(!booleanTester.test(this.value.searchable) ){
				this.addError({ 'searchable': ['Please, select if the column is searchable.']});
				isValid = false;
			}
		}

		if( this.value.orderable != '' ){
			if(!booleanTester.test(this.value.orderable) ){
				this.addError({ 'orderable': ['Please, select if the column is orderable.']});
				isValid = false;
			}
		}
		return isValid;
	}
};