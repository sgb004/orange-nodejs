/**
 * @author @sgb004
 * @version 0.1.0
 */

var OrderType = function(){
	this.value = {};
	this.errors = [];
	this.required = true;
}
var functions = require('./OrderType');
OrderType.prototype = functions.functions;
OrderType.prototype.addError = function( notice ){this.errors.push( notice );}
OrderType.prototype.getErrors = function( notice ){return this.errors;}

module.exports.functions = {
	setValue: function(value){
		this['isObject'] = true;
		if( typeof(value) != 'object' ){
			this.isObject = false;
		}
		this.value = value;
	},
	isEmpty: function(){
		var isEmpty = false;
		var value = {};
		var length = 0;
		var i;
		if( this.isObject ){
			value = this.value;
		}

		for( i in value ){
			length++;
			break;
		}

		if( length == 0 ){
			isEmpty = true;
			if( this.required ){
				this.addError(this.notice);
			}
		}
		this['_isEmpty'] = isEmpty;
		return isEmpty;
	},
	validate: function() {
		var isValid = true;
		if( !this._isEmpty ){
			if( this.isObject ){
				var order, c, fieldErrors;
				var errors = {};
				for( c in this.value ){
					fieldErrors = [];
					order = new OrderType();
					order.setValue(this.value[c]);
					this.value[c] = order.value;
					if( order.isEmpty() ){
						fieldErrors = order.getErrors();
					}else if( !order.validate() ){
						fieldErrors = order.getErrors();
					}
					if( fieldErrors.length > 0 ) {
						isValid = false;
						errors[c] = [ fieldErrors ];
					}
				}
				this.addError(errors);
			}else{
				isValid = false;
				this.addError('Please, add a order direction.');
			}
		}
		return isValid;
	}
};