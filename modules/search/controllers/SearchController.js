/**
 * @author @sgb004
 * @version 0.1.1
 */

class SearchController{
	search(requestBody, searchModel){
		return new Promise( (resolve, reject ) => {
			var path = __dirname;
			path = path.replace(process.cwd(), '');
			path += '/fields/';
			var data = {
				'success': false,
				'notice': 'Check fields with errors.',
				'recordsTotal': 0,
				'recordsFiltered': 0,
				'data': {},
				'fields': {}
			}
			var form = require('../../form/controllers/Form');
			form = new form();
			form.name = 'search';
			form.fields = {
				'draw': {
					'type': 'number'
				},
				'columns': {
					'params_file': path+'ColumnsType',
					'type': 'columns'
				},
				'order': {
					'params_file': path+'OrderListType',
					'type': 'orderList'
				},
				'start': {
					'type': 'number'
				},
				'length': {
					'type': 'number'
				},
				'search': {
					'params_file': path+'SearchType',
					'type': 'search'
				}
			};

			form.submit(requestBody)
			.then( isValid => {
				data.success = isValid;
				if( isValid ){
					var start = (form.fields.start.value == '') ? 0 : form.fields.start.value;
					var length = (form.fields.length.value == '' ) ? 0 : form.fields.length.value;
					data.notice = '';
					
					searchModel.config = {
						'columns': form.fields.columns.value,
						'order': form.fields.order.value,
						'start': start,
						'length': length,
						'search': form.fields.search.value
					};
					return searchModel.getAllData();
				}else{
					data.fields = form.getErrorsFields();
					return data;
				}
			})
			.then( _data => {
				data.recordsTotal = _data.recordsTotal;
				data.recordsFiltered = _data.recordsFiltered;
				data.data = _data.data;
				resolve(data);
			})
			.catch( error => {
				reject(error);
			});
		});
	}
}

module.exports = SearchController;