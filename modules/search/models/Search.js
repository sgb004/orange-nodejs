/**
 * @author @sgb004
 * @version 0.1.9
 */

const DB = require (process.cwd()+'/node_modules/orange-nodejs/libs/DB');

class Search extends DB{
	constructor(config){
		super();

		this.table = '';
		this.config = config;
		this.default = {
			'order': '',
			'length': 25
		};
		this.columnsIgnore = {}

		//this.columns = '';
		this.joins = '';
		this.conditions = '';
		this.parameters = [];
		this.searchColumns = {};
		this.groupBy = '';
		
		this.useHaving = false;
		this.having = [];
		this.havingColumns = {};
		this.havingColumnsQuery = '';
		this.havingConditions = '';
	}

	getAllData(){
		return new Promise( (resolve, reject) => {
			var data = {
				'recordsTotal': 0,
				'recordsFiltered': 0,
				'data': {}
			};
			var order = '';

			this.allLength()
			.then( total => {
				data.recordsTotal = total;
				return this.getResult('SHOW COLUMNS FROM '+this.table, []);
			})
			.then( _columns => {
				var c;
				var columns = {};
				var name, column;
				var search = ''+this.config.search.value;
				var filtered = false;

				search = search.trim();
				for( c in _columns ){
					if( this.columnsIgnore[_columns[c].Field] == undefined ){
						columns[_columns[c].Field] = _columns[c].Type;
					}
				}
				_columns = null;
				columns = Object.assign(this.searchColumns, columns);

				if( search != '' ){
					var havingColumns = {};
					filtered = true;
					for( c in this.havingColumns ){
						column = '('+this.havingColumns[c].query+')';
						havingColumns[column] = this.havingColumns[c].type;
					}
					havingColumns = Object.assign(columns, havingColumns);
					this.setSearch( havingColumns, search );
				}

				for( c in this.config.columns ){
					column = this.config.columns[c];
					search = ''+column.search.value;
					search = search.trim();
					if( search != '' ){
						search = '%'+search+'%';
						name = (column.name == '') ? column.data : this.config.columns.name;
						if( columns[name] != undefined ){
							filtered = true;
							this.addCondition(name+' LIKE ?');
							this.parameters.push( search );
						}

						if( this.havingColumns[name] != undefined ){
							filtered = true;
							this.addHavingCondition('('+this.havingColumns[name].query+') LIKE ?');
							this.having.push(name+' LIKE ?');
							this.parameters.push( search );
						}
					}
				}

				order = [];
				for( c in this.config.order ){
					column = this.config.columns[this.config.order[c].column];
					if( column != undefined ){
						name = (column.name == '') ? column.data : column.name;
						if( columns[name] != undefined || this.havingColumns[name] != undefined ){
							order.push(name+' '+this.config.order[c].dir);
						}
					}
				}
				order = order.join(' , ');

				if( order == '' ){
					order = this.default.order;
				}

				filtered = this.addFilters(filtered);

				if( filtered ){
					return this.allLength();
				}else{
					return data.recordsTotal;
				}
			})
			.then( total => {
				var length = ( this.config.length == 0 ) ? this.default.length : this.config.length;
				data.recordsFiltered = total;
				return this.getAllMultiOrder(this.allQuery(), order, this.config.start, length);
			})
			.then( _data => {
				data.data = _data;
				resolve(data);
			})
			.catch( error => {
				reject(error);
			})
		});
	}

	allLength(){
		return this.getAllLength( '' );
	}

	getAllLength( query ){
		return new Promise( (resolve, reject) => {
			query = query+this.joins+this.conditions+this.havingConditions;
			this.openConnection();
			this.getResult( query, this.parameters ).then( rows => {
				if( rows.length == 0 ){
					resolve(0);
				}else{
					resolve(rows[0].total);
				}
			})
			.catch( error => {
				console.log( error );
				reject(error);
			});
		});
	}

	allQuery(){
		return '';
	}

	all( order, dir, start, length ){
		return this.getAll( this.allQuery(), order, dir, start, length );
	}

	getAll( query, order, dir, start, length ){
		return this.getAllMultiOrder(query, order+' '+dir, start, length );
	}

	getAllMultiOrder(query, order, start, length){
		var having = '';
		if( this.useHaving ){
			var pos = query.lastIndexOf('FROM');
			if( this.having.length > 0 ){
				having = ' HAVING '+this.having.join(' AND ');
			}
			this.setHavingColumns();
			query = query.substring(0, pos)+' '+this.havingColumnsQuery+' '+query.substring(pos);
		}
		if( order != '' ){ order = ' ORDER BY '+order; }
		query = query+this.joins+this.conditions+this.groupBy+having+order+' LIMIT '+start+','+length;
		return this.getResult( query, this.parameters );
	}

	addSearch( text ){
		this.setSearch({}, text);
	}

	setSearch( columns, text ){
		var search = '%'+text+'%';
		var key, isValidDate, _searchDate, month;
		var query = '';
		var searchDate = '';
		for( key in columns ){
			if( columns[key] == 'date' || columns[key] == 'datetime'  ){
				if( searchDate == '' ){
					isValidDate = true;
					searchDate = text.split(' ');
					searchDate[0] = searchDate[0].split('-');
					if( searchDate[0][1] == undefined ){
						isValidDate = false;
					}else if( searchDate[0][2] == undefined ){
						isValidDate = false;
					}else{
						month = parseInt(searchDate[0][1]);
						if( isNaN(month) ){
							isValidDate = false;
						}else{
							month -= 1;
						}
					}

					if( isValidDate ){
						if( searchDate[1] == undefined ){
							searchDate[1] = ['00', '00', '00'];
						}else{
							searchDate[1] = searchDate[1].split(':');
							if(searchDate[1][1] == undefined){
								searchDate[1].push('00');
								searchDate[1].push('00');
							}else if(searchDate[1][2] == undefined){
								searchDate[1].push('00');
							}
						}

						_searchDate = new Date( searchDate[0][2], month, searchDate[0][0], searchDate[1][0], searchDate[1][1], searchDate[1][2] );

						if( _searchDate.toString() == 'Invalid Date' ){
							searchDate = 'Invalid Date';
						}else{
							searchDate = searchDate[0][2]+'-'+searchDate[0][1]+'-'+searchDate[0][0]+' '+searchDate[1][0]+':'+searchDate[1][1]+':'+searchDate[1][2];
						}
					}else{
						searchDate = 'Invalid Date';
					}
				}

				if( searchDate != 'Invalid Date' ){
					query += key+' >= ? OR ';
					this.parameters.push(searchDate);
				}
			}else{
				query += key+' LIKE ? OR ';
				this.parameters.push(search);
			}
		}
		if( query != '' ){
			query = query.substring(0, query.length-4);
			query = '('+query+')';
			this.addCondition(query);
		}
	}

	addCondition( condition ){
		if( this.conditions == '' ){
			this.conditions += ' WHERE 1=1 ';
		}
		this.conditions += ' AND '+condition;
	}

	addHavingCondition( condition ){
		if( this.conditions == '' ){
			this.conditions += ' WHERE 1=1 ';
		}
		this.havingConditions += ' AND '+condition;
	}

	setHavingColumns(){
		var c;
		var query = '';
		for( c in this.havingColumns ){
			query += ', ('+this.havingColumns[c].query+') AS '+c;
		}
		this.havingColumnsQuery = query;
	}

	addFilters(filtered){
		return filtered;
	}
}

module.exports = Search;