/**
 * @author @sgb004
 * @version 0.2.5
 * @description Permite procesar modulos de forma fácil
 */

function Field( params, functions ){
	this.empty = null;
	this.name = '';
	this.value = '';
	this.required = false;
	this.notice = 'This field is required.';
	this.pattern = '';
	this.patternError = '';
	this.minlength = 0;
	this.maxlength = 0;
	this.notlengthError = 'The number of characters this field must have ',
	this.minlengthError = 'The minimum number of characters is ',
	this.maxlengthError = 'The maximum number of characters is ',
	this.errors = [];
	for( p in params ){
		this[p] = params[p];
	}
	this.init( functions );
	return this;
}

Field.prototype = {
	init: function( functions ){
		for( fn in functions ){
			this[fn] = functions[fn];
		}
	},
	setValue: function( value ){
		value = ""+value;
		this.value = value.trim();
	},
	isEmpty: function(){
		var isEmpty = false;
		if( this.required ){
			if( this.value == '' ){
				isEmpty = true;
				this.addError(this.notice);
			}
		}
		return isEmpty;
	},
	validate: function(){
		var isValid = true;

		if( this.value != '' ){
			if( this.pattern != '' ){
				var p = new RegExp(this.pattern);
				isValid = p.test(this.value);
				if( !isValid ){
					this.addError( this.patternError );
				}
			}

			isValid = this.validateMinMax(isValid);
		}

		return isValid;
	},
	validateMinMax: function(isValid){
		if( this.minlength > 0 || this.maxlength > 0 ){
			if( this.minlength > 0 && this.maxlength > 0 && this.minlength == this.maxlength && this.value.length != this.minlength ){
				this.addError( this.notlengthError+this.minlength );
				isValid = false;
			}else if( this.minlength > 0 && this.value.length < this.minlength ){
				this.addError( this.minlengthError+this.minlength );
				isValid = false;
			}else if( this.maxlength > 0 && this.value.length > this.maxlength ){
				this.addError( this.maxlengthError+this.maxlength );
				isValid = false;
			}
		}
		return isValid;
	},
	addError: function( notice ){
		this.errors.push( notice );
	},
	getErrors: function( notice ){
		return this.errors;
	}
}

class Form{
	constructor(){
		this.name = 'form';
		this.fields = {};
		this.fieldsErrors = {};
		this.filters = {};
		this._isValid = true;
	}

	submit(data){
		var key, field, value, isEmpty, k, required, fieldTypeParamsFile;
		var fields = {};
		var defaultParams = [ 'notice', 'pattern', 'patternError', 'minlength', 'maxlength', 'notlengthError', 'minlengthError', 'maxlengthError', 'choices', 'choiceError', 'isMultiple' ];
		var dp;

		this._isValid = false;

		if( typeof(data) != 'object' ){ data = {}; }

		for( key in this.fields ){
			value = ( data[key] == undefined ) ? '' : data[key];
			field = this.fields[key].type;
			field = field.charAt(0).toUpperCase() + field.slice(1);
			if( typeof(this.fields[key]['params_file']) == 'string' ){
				field = require(process.cwd()+this.fields[key]['params_file']);
			}else{
				field = require('./fields/'+field+'Type');
			}
			fields[key] = new Field( field.params, field.functions );
			fields[key].name = key;
			fields[key].type = this.fields[key].type;
			required = ( this.fields[key].required == true ) ? true : false;
			fields[key].required = Boolean(required);

			dp = (typeof(field.defaultParams) == 'object' ) ? defaultParams.concat(field.defaultParams) : defaultParams;
			for( k in dp ){
				if( this.fields[key][dp[k]] == undefined || this.fields[key][dp[k]] == null ){ continue; }
				fields[key][dp[k]] = this.fields[key][dp[k]];
			}

			fields[key].setValue( value );
		}

		return new Promise( (resolve, reject) => {
			this._isValid = true;
			this.applyFilter('pre_validate_data', 'first', fields).then( fields => {
				for( key in fields ){
					isEmpty = fields[key].isEmpty();
					fields[key].empty = isEmpty;
					if( isEmpty && fields[key].required ){
						this._isValid = false;
					}
				}

				if( this._isValid ){
					return this.applyFilter( 'post_set_data', 'first', fields );
				}else{
					var errors;
					for( key in fields ){
						errors = fields[key].getErrors();

						if( errors.length > 0 ){
							this.fieldsErrors[key] = errors;
						}
					}
					resolve( this._isValid );
					return Promise.reject(false);
				}
			})
			.then( fields => {
				var isValid;
				var l = [];
				
				for( key in fields ){
					l.push( fields[key].validate() );
				}

				Promise.all( l ).then( values => {
					var i;
					for( i in values ){
						if( !values[i] ){
							this._isValid = false;
						}
					}

					this.applyFilter('post_validate_data', 'all', this._isValid, fields ).then( isValidList => {
						var v;
						isValid = true;
						if( typeof( isValidList ) == 'object' ){
							for( v in isValidList ){
								if( !isValidList[v] ){
									isValid = false;
									break;
								}
							}
						}else{
							isValid = isValidList;
						}
						this._isValid = isValid;

						if( this._isValid ){
							for( key in fields ){
								this.fields[key].value = fields[key].value;
							}
						}else{
							var errors;

							for( key in fields ){
								errors = fields[key].getErrors();

								if( errors.length > 0 ){
									this.fieldsErrors[key] = errors;
								}
							}
						}
						resolve( this._isValid );
					})
					.catch( error => {
						reject(error);
					});
				}, error => {
					reject(error);
				});
			})
			.catch( error => {
				if( error != false ){
					reject(error);
				}
			});
		});

	}

	isValid(){
		return this._isValid;
	}

	getNotice(){
		return '';
	}

	getErrorsFields(){
		return this.fieldsErrors;
	}

	addFilter( filter, fn ){
		if( this.filters[filter] == undefined ){
			this.filters[filter] = [];
		}
		this.filters[filter].push(fn);
	}

	applyFilter( filter, type, p0, p1 ){
		return new Promise((resolve, reject) => {
			if( this.filters[filter] == undefined ){
				resolve(p0);
			}else{
				var i;
				var l = [];

				for( i=0; i<this.filters[filter].length; i++ ){
					l.push(this.filters[filter][i]( p0, p1 ));
				}

				Promise.all( l ).then( values => {
					if( type == 'all' ){
						resolve( values );
					}else{
						resolve( values[0] );
					}
				}, error => {
					reject(error);
				});
			}
		});
	}
}

module.exports = Form;