/**
 * @author @sgb004
 * @version 0.1.1
 */

module.exports.params = {
	choices: {},
	choiceError: 'Selected option is not available.',
	isMultiple: false
};
module.exports.functions = {
	setValue: function( value ) {
		value = ""+value;
		value = value.trim();
		if( this.isMultiple ){
			//TODO: Version multiple
		}else{
			this.value = value;
		}
	},
	validate: function() {
		var isValid = true;
		if( this.isMultiple ){
			//TODO: Version multiple
		}else{
			if( this.choices[ this.value ] == undefined || this.choices[ this.value ] == null ){
				this.addError(this.choiceError);
				isValid = false;
			}
		}
		return isValid;
	},
	isEmpty: function() {
		var isEmpty = false;
		if( this.required ){
			if( this.isMultiple ){
				//TODO: Version multiple
			}else if( this.value == '' ){
				this.addError( this.notice );
			}
		}
		return isEmpty;
	}
};