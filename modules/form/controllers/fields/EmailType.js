/**
 * @author @sgb004
 * @version 0.1.0
 */

module.exports.params = {
	pattern: '^[a-z0-9-_.+%]+@[a-z0-9-.]+\.[a-z]{2,4}$',
	patternError: 'Please, write a correct email account.',
};
module.exports.functions = {};