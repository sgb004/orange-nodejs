/**
 * @author @sgb004
 * @version 0.1.1
 */

module.exports.params = {
	pattern: '^[0-9]+$',
	patternError: 'Please, write only numbers.',
};
module.exports.functions = {
	setValue: function( value ){
		value = ""+value;
		this.value = value.trim();

		if( !this.required && this.value == '' ){
			this.value = 0;
		}
	},
};