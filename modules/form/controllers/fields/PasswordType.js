/**
 * @author @sgb004
 * @version 0.1.1
 */

module.exports.params = {
	minlength: 8
};
module.exports.functions = {
	validate: function() {
		var isValid = true;

		if( this.value != '' ){
			var p = /[0-9]/;

			isValid = this.validateMinMax(isValid);

			if( !p.test(this.value) ){
				this.addError('Please, include numbers in the password.');
				isValid = false;
			}

			p = /[a-zA-Z]/;
			if( !p.test(this.value) ){
				this.addError('Please, include letters in the password.');
				isValid = false;
			}

			p = /^[0-9a-zA-Z]+$/;
			if( p.test(this.value) ){
				this.addError('Please, include special char in the password.');
				isValid = false;
			}
		}

		return isValid;
	}
};