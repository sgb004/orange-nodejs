/**
 * @author @sgb004
 * @version 0.1.7
 */

class ExampleControllers{
	indexAction(request, result) {
		result.send('Welcome to Orange to Node.js');
	}

	registerAction(request, result) {
		var r = {success: false, notice: '', fields: {}}; 
		var form = require('../../form/controllers/Form');
		form = new form();
		form.name = 'register';
		form.fields = {
			'full_name': {
				'type': 'text',
				'required': true
			},
			'email': {
				'type': 'email',
				'required': true
			},
			'password': {
				'type': 'password',
				'required': true
			},
			'accept_terms': {
				'type': 'choice',
				'required': true,
				'choices': {
					"1": "1"
				}
			}
		};

		//Filtro de campos despues de que la informacion fue agregada
		form.addFilter('pre_validate_data', function(fields){
			console.log( 'FILTRO pre_validate_data:' );
			console.log( fields );
			console.log( '-------------------------' );
			return fields;
		});

		//Filtro de campos despues de que la informacion fue validada
		form.addFilter('post_validate_data', function(isValid, fields){
			console.log( 'FILTRO post_validate_data:' );
			console.log( isValid );
			console.log( fields );
			console.log( '-------------------------' );
			return isValid;
		});

		form.submit( request.body );
		if( form.isValid() ){
			console.log( 'FORMULARIO CORRECTO' );
			console.log( form.fields );
		}else{
			r.notice = form.getNotice();
			r.fields = form.getErrorsFields();
		}

		result.setHeader('Content-Type', 'application/json');
		result.send(JSON.stringify(r));
	}

	userAction(request, result) {
		if( request.params.id == undefined ){
			result.send('Select an user');
		}else{
			result.send('Hello '+request.params.id+'!');
		}
	}

	tokenMakeAction(request, result) {
		var token = require('../../orange/controllers/TokensController');
		token = new token();
		token.make( '', '', {}, function( t ){
			console.log( 'TOKEN GENERADO' );
			console.log( t );
			result.send('Test token '+t);
		}, function( error ){
			console.log( 'ERROR AL GENERAL EL TOKEN' );
			console.log( error );
			result.send('ERROR');
		});
	}

	tokenCheckAction(request, result) {
		if( request.params.token == undefined ){
			result.send('Write a token');
		}else{
			var token = require('../../orange/controllers/TokensController');
			token = new token();
			token.check( request.params.token, '' ).then( isValid => {
				result.send('Is a valid token? '+isValid);
			}).catch( error => {
				console.log( error );
				result.send('ERROR');
			});
		}
	}

	passwordMakeAction(request, result) {
		if( request.params.password == undefined ){
			result.send('Write a password');
		}else{
			var password = require('../../../libs/Password'); 
			password = new password();
			password = password.make(request.params.password);
			result.send('Password encrypted '+password);
		}
	}

	passwordCheckAction(request, result) {
		if( request.params.password == undefined || request.params.key == undefined ){
			result.send('Write a password and key');
		}else{
			var password = require('../../../libs/Password');
			var match = false;
			password = new password();
			match = password.check(request.params.password, request.params.key);
			if( match ){
				result.send('The password matches');
			}else{
				result.send('The password doesn\'t match');
			}
		}
	}
}

module.exports = ExampleControllers;