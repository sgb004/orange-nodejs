/**
 * @author @sgb004
 * @version 0.1.3
 */

const DB = require ('../../../libs/DB');

class Tokens extends DB{
	constructor(){
		super();
		this.token = '';
		this.type = '';
		this.limitTime = '';
		this.table = 'tokens';
	}

	getToken(){
		return this.getResult( 'SELECT * FROM '+this.table+' WHERE token_key = ?', [this.token] );
	}

	addToken(){
		var date = new Date();
		var data = {
			'token_key': this.token,
			'type': this.type,
			'register_date': global.dateToFullString(date)
		};
		return this.add( this.table, data );
	}

	removeByType(){
		return this.delete( this.table, { 'type =': this.type, 'register_date < ': this.limitTime } );
	}
}

module.exports = Tokens;