/**
 * @author @sgb004
 * @version 0.2.5
 */

let limitTime = 60;
let tokensModel = '../models/Tokens';

class TokensControllers{
	constructor(){
		this.limitTime = 60;
		this.tokensModel = '../models/Tokens';
	}

	make( phrase, type, data, resolve, reject ){
		var md5 = require('md5');
		var tokens = require(this.tokensModel);
		var date = new Date();
		var key;
		var p = require ('../../../libs/Password');
		if( phrase == undefined || phrase == null ){ phrase = ''; }
		if( type == undefined || type == null ){ type = ''; }
		if( data == undefined || data == null ){ data = {}; }

		phrase = phrase+'~'+global.dateToString(date)+'~'+global.dateToTimeString(date)+'~'+Math.floor((Math.random() * 100) + 1);
		phrase = md5(phrase);
		p = new p();
		phrase = p.make(phrase);

		tokens = new tokens();
		tokens.token = phrase;
		tokens.type = type;
		tokens.limitTime = this.makeLimitTime();

		for( key in data ){
			tokens[key] = data[key];
		}

		tokens.removeByType().then(() => {
			return tokens.getToken();
		}).then( rows =>{
			if( rows.length == 0 ){
				tokens.addToken().then( insertId => {
					resolve( phrase );
				}).catch( error => {
					reject( error );
				});
			}else{
				this.make( phrase, type, resolve, reject );
			}
		}).catch( error => {
			reject( error );
		});
	}

	makeLimitTime(){
		var date = new Date();
		var limitTime = new Date( date.getTime() - ( this.limitTime * 60000 ) );
		return global.dateToFullString(limitTime);
	}

	getToken( phrase, type ){
		return new Promise( (resolve, reject) =>{
			var tokens = require(this.tokensModel);

			if( type == undefined || type == null ){ type = ''; }

			tokens = new tokens();
			tokens.token = phrase;
			tokens.type = type;
			tokens.limitTime = this.makeLimitTime();

			tokens.removeByType().then(() => {
				return tokens.getToken();
			}).then( rows =>{
				resolve( rows );
			}).catch( error => {
				reject( error );
			});
		});
	}

	check( phrase, type ){
		return new Promise( (resolve, reject) =>{
			this.getToken(phrase, type).then( rows => {
				if( rows.length > 0 ){
					resolve( true );
				}else{
					resolve( false );
				}
			}).catch( error => {
				reject( error );
			});
		});
	}

	getUid( phrase, type ){
		return new Promise( (resolve, reject) =>{
			this.getToken(phrase, type).then( rows => {
				if( rows.length == 0 ){
					resolve( 0 );
				}else{
					resolve( rows[0].uid );
				}
			}).catch( error => {
				reject( error );
			});
		});
	}
}

module.exports = TokensControllers;