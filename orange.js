/**
 * @author @sgb004
 * @version 0.1.7
 */

const abspath = process.cwd();
var _routes = require(abspath+'/routes.json');
var t;
const routes = { 'GET': {}, 'POST': {} };
let express = require('express');
let bodyParser = require('body-parser');
let site = express();
let config = require(process.cwd()+'/config.js');
let port = process.env.PORT || config.PORT || 4000;

site.use(function (request, result, next){
	result.setHeader('Access-Control-Allow-Origin', '*');
	result.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	result.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	result.setHeader('Access-Control-Allow-Credentials', true);
	next();
});
site.use( bodyParser.json({ limit: config.sizeLimit }) );
site.use( bodyParser.urlencoded({ extended: true, limit: config.sizeLimit }) );
for( use in config.uses ){
	site.use( config.uses[use] );
}

for( type in _routes ){
	t = type.toUpperCase();
	for( route in _routes[type] ){
		for( p in _routes[type][route].paths ){
			routes[t][_routes[type][route].paths[p]] = _routes[type][route];
			site[type](_routes[type][route].paths[p], function(request, result){
				var controller = require(abspath+routes[request.method][request.route.path].controller);
				controller = new controller();
				var view = routes[request.method][request.route.path].view+'Action';
				controller[view]( request, result );
			});
		}
	}
}
_routes = undefined;
t = undefined;

global.dateToString = function( date ){
	date = date.toISOString();
	date = date.split('T');
	return date[0];
};

global.dateToTimeString = function( date ){
	date = date.toISOString();
	date = date.split('T');
	date = date[1].split('.');
	return date[0];
};

global.dateToFullString = function( date ){
	date = date.toISOString();
	date = date.split('T');
	date[1] = date[1].split('.');
	date[1] = date[1][0];
	date = date[0]+' '+date[1];
	return date;
};

site.listen(port, function(){
	console.log('Ready to win!');
});