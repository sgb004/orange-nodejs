module.exports.db = {
	host: 'localhost', 
	user: 'root', 
	password: '', 
	database: 'orange_nodejs', 
};

module.exports.passwordPosition = 29;

module.exports.sizeLimit = '50mb';

module.exports.PORT = 4000;