/**
 * @author @sgb004
 * @version 0.1.4
 */

const mysql = require('mysql');

class DB{
	constructor(){
		this.openConnection();
	}

	openConnection(){
		if( global['db'] == undefined ){
			let config = require(process.cwd()+'/config.js');
			global['db']  = mysql.createConnection( config.db );
		}
		this.connection = global['db'];
	}

	add( table, data ){
		this.openConnection();
		this.insertId = 0;
		return new Promise( (resolve, reject) => {
			this.connection.query( 'INSERT INTO '+table+' SET ?', data, ( error, result ) =>{
				if( error != null ){
					reject( error );
				}else{
					resolve( result.insertId );
				}
			});
		});
	}

	update( table, data, conditions ){
		this.openConnection();
		return new Promise( (resolve, reject) => {
			var query = 'UPDATE '+table+' SET ';
			var k;
			var d = [];
			var c = ' WHERE 1=1 ';

			for( k in  data ){
				query += k+' = ?, ';
				d.push( data[k] );
			}
			query = query.substring(0, query.length-2);

			query += c;

			for( k in conditions ){
				query += ' AND '+k+' ?';
				d.push( conditions[k] );
			}

			this.connection.query( query, d, ( error, result ) =>{
				if( error != null ){
					reject( error );
				}else{
					resolve();
				}
			});
		});
	}

	getResult( query, data ){
		this.openConnection();
		return new Promise( (resolve, reject) => {
			this.connection.query( query, data, ( error, rows ) =>{
				if( error != null ){
					reject( error );
				}else{
					resolve( rows );
				}
			});
		});
	}

	delete( table, data ){
		this.openConnection();

		var c = ' WHERE 1=1';
		var k;
		var d = [];
		for( k in data ){
			c += ' AND '+k+' ?';
			d.push( data[k] )
		}

		return new Promise( (resolve, reject) => {
			this.connection.query( 'DELETE FROM '+table+c, d, ( error, results ) =>{
				if( error != null ){
					reject( error );
				}else{
					resolve();
				}
			});
		});
	}
}

module.exports = DB;