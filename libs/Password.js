/**
 * @author @sgb004
 * @version 0.1.0
 */

class Password{
	constructor(){
		let config = require(process.cwd()+'/config.js');

		this.letter = config.passwordPosition;
		this.letters = ['_', '&', '.', '*', ';', '#', '%', '-', '$'];
		this.lettersSize = this.letters.length - 1;
	}

	/**
	 * Funciones que eligen de forma aleatoria un caracter para ser agregados a la contraseña
	 */
	rdp(){ return String.fromCharCode( randomMinMax(48, 57) ); }
	rds(){ return String.fromCharCode( randomMinMax(65, 90) ); }
	rdt(){ return String.fromCharCode( randomMinMax(97, 122) ); }

	/**
	 * Crea la contraseña
	 *
	 * @param password string la contraseña a codificar
	 * @return string devuelve la contraseña codificada
	 */
	make( password ){
		var md5 = require('md5');
		var rd = randomMinMax(0, 3);

		password = this.letters[randomMinMax(0, this.lettersSize)]+password+this.letters[randomMinMax(0, this.lettersSize)];
		password = md5(md5(md5(password)));
		if(rd == 1){rd = this.rdp();}
		else if(rd == 2){rd = this.rds();}
		else{rd = this.rdt();}

		password = password.substring(0, this.letter)+rd+password.substring(this.letter);
		return password;
	}

	/**
	 * Compara la contraseña con algun string
	 *
	 * @param passwordPlain string con la contraseña a comprobar
	 * @param password string la contraseña codificada por la funcion make
	 * @return boolean
	 */
	check( passwordPlain, password ){
		var md5 = require('md5');
		var letterNext = this.letter+1;
		var match = false;
		var i, j, passwordCoded;

		passwordPlain = passwordPlain.trim();
		password = password.trim();
		password = password.substring(0, this.letter)+password.substring(letterNext);
		for( i=0; i<this.letters.length; i++ ){
			for( j=0; j<this.letters.length; j++ ){
				passwordCoded = this.letters[j]+passwordPlain+this.letters[i];
				passwordCoded = md5(md5(md5(passwordCoded)));
				if(passwordCoded == password){
					match = true;
					break;
				}
			}
			if( match ){
				break;
			}
		}

		return match;
	}
}

module.exports = Password;

/**
 * @from https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Math/random
 */
function randomMinMax(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}